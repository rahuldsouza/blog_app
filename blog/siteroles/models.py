from django.db import models

from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager

from profiles.models import Profile
from roles.models import Role


class SiteProfileRole(models.Model):
    site = models.ForeignKey(Site, related_name='profiles')
    profile = models.ForeignKey(Profile, related_name='site_roles')
    role = models.ForeignKey(Role, related_name='site_profiles')
    objects = models.Manager()
    # on_site = CurrentSiteManager()

    class Meta:
        verbose_name = "SiteProfileRole"
        verbose_name_plural = "SiteProfileRole"

    def __str__(self):
        return 'S: %s |P: %s |R: %s' % (self.site, self.profile, self.role)
#     