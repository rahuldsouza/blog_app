import json

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.sites.shortcuts import get_current_site
# Create your views here.
from siteroles.models import *

def testView(request):
    site_roles = SiteProfileRole.objects.filter(site=request.site)
    response = {role.profile.user.username : role.role.title for role in site_roles}
    return HttpResponse( json.dumps(response), content_type="application/json")