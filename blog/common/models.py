from django.db import models
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel

# Create your models here.
class Account(TimeStampedModel, TitleDescriptionModel):
    """
    Description: Model Description
    """
    pass

    class Meta:
    	verbose_name = "Account"
        verbose_name_plural = "Accounts"
    
    def __str__(self):
        return self.title