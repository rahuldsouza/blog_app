import uuid

from django.db import models

from profiles.models import Profile
from django.contrib.sites.models import Site
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel
from common.models import Account

class Tag(TitleDescriptionModel, TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pass

    class Meta:
        verbose_name='Tag'
        verbose_name_plural = 'Tags'

    def __str__(self):
        return self.title

class Post(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    site = models.ForeignKey(Site, related_name='posts')
    profile = models.ForeignKey(Profile, related_name='posts')
    content = models.TextField()
    tags = models.ManyToManyField(Tag, related_name='posts')
    account = models.OneToOneField(Account)

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts" 
    
