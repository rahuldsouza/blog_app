from django.db import models

from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel
# Create your models here.

class Permission(TitleDescriptionModel):
    class Meta:
        verbose_name = "Permission"
        verbose_name_plural = "Permissions"

    def __str__(self):
        return self.title

class Role(TitleDescriptionModel):
    class Meta:
        verbose_name = "Role"
        verbose_name_plural = "Roles"

    def __str__(self):
        return self.title

class RolePermission(models.Model):
    role = models.ForeignKey(Role, related_name='permissions')
    permission = models.ForeignKey(Permission, related_name='roles')

    class Meta:
        verbose_name = "RolePermission"
        verbose_name_plural = "RolePermissions"

    def __str__(self):
        return '%s | %s' % (self.role, self.permission)
    