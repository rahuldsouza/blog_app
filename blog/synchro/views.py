import json

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.core import serializers
from django.template.response import TemplateResponse
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _

from synchro import call_synchronize, reset_synchro
from synchro.models import options
from synchro import settings
from .models import ChangeLog, ADDITION, CHANGE, DELETION, M2M_CHANGE


@staff_member_required
def synchro(request):
    if 'synchro' in request.POST:
        try:
            msg = call_synchronize()
            messages.add_message(request, messages.INFO, msg)
        except Exception as e:
            if settings.DEBUG:
                raise
            msg = _('An error occured: %(msg)s (%(type)s)') % {'msg': str(e),
                                                               'type': e.__class__.__name__}
            messages.add_message(request, messages.ERROR, msg)
    elif 'reset' in request.POST and settings.ALLOW_RESET:
        reset_synchro()
        msg = _('Synchronization has been reset.')
        messages.add_message(request, messages.INFO, msg)
    return TemplateResponse(request, 'synchro.html', {'last': options.last_check,
                                                      'reset_allowed': settings.ALLOW_RESET})


def dump_changelogs_json():
    """
    {
        "types":["type-1", "type-2"],
        "type-1":{
            "create":[obj, obj],
            "update":[],
            "delete":[]
        },
        "type-2":{
            "create":[obj, obj],
            "update":[],
            "delete":[]
        },
        
    }
    """
    changelog_dump = {}
    changelogs = ChangeLog.objects.all()
    log_types = set()
    for changelog in changelogs.iterator():
        log_types.add(changelog.content_type.model)
        if not changelog.content_type.model in changelog_dump:
            changelog_dump[changelog.content_type.model] = {ADDITION:[], CHANGE:[], DELETION:[], M2M_CHANGE:[]}
        if changelog.action == DELETION:
            changelog_dump[changelog.content_type.model][changelog.action].append(changelog.object_id)    
        else:
            changelog_dump[changelog.content_type.model][changelog.action].extend(json.loads(changelog.serialized_data))
    changelog_dump["types"] = list(log_types)
    with open('changelog.json', 'w') as outfile:
        json.dump(changelog_dump, outfile)


def import_changelogs_json():
    with open('changelog.json') as data_file:    
        changelog_import = json.load(data_file)
    for object_type in changelog_import["types"]:
        insert_import = changelog_import[object_type][str(ADDITION)]
        for deserialized_object in serializers.deserialize("json", json.dumps(insert_import)):
            deserialized_object.save()

        change_import = changelog_import[object_type][str(CHANGE)]
        for deserialized_object in serializers.deserialize("json", json.dumps(change_import), ignorenonexistent=True):
            deserialized_object.save()
        
        m2m_import = changelog_import[object_type][str(M2M_CHANGE)]
        for deserialized_object in serializers.deserialize("json", json.dumps(m2m_import), ignorenonexistent=True):
            deserialized_object.save()

        deletion_import = changelog_import[object_type][str(DELETION)]
        model_object = ContentType.objects.get(model=object_type).model_class()
        model_object.objects.filter(id__in=deletion_import).delete()

