import uuid

from django.db import models
from django.contrib.auth.models import User

from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel

from roles.models import Role
from common.models import Account

# Create your models here.
class Profile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User)
    account = models.ForeignKey(Account)

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    def __str__(self):
        return self.user.username
    